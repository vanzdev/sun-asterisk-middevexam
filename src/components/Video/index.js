import React from 'react';

const Video = () => {
  return(
    <div className="video-wrapper">
      <MainVideo />
      <VideoList />
    </div>
  )
}

const MainVideo = () => {
  return(
    <div className="main-video">
      <div className="main-video__thumbnail"></div>
      <h2 className="main-video__title">Video Title Here</h2>
      <p className="main-video__desc">lorem ipsum dolor sit amet</p>
    </div>
  )
}

const VideoList = () => {
  return(
    <div className="video-list">
      <div className="video-item">
        <div className="video-item__thumbnail"></div>
        <h4 className="video-item__title">Video Title</h4>
      </div>
    </div>
  );
}

export default Video;