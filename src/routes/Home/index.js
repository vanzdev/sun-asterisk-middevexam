import React from 'react';
import Search from '../../components/Search';
import Video from '../../components/Video';

const Home = () => {
  return(
    <>
      <Search />
      <Video />
    </>
  );
};

export default Home;